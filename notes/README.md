notes
=====

The notes script help to make notes in your preferred editor. This script uses
gvim as default editor.

It has three modes:

- persistent
	This is the default mode and save the note in a subdirectory of the
	base directory and is persistent.

- temporary
	The notes file is saved in a ram disk.

- shredding
	The notes file is also saved in a ram disk, but will be shred and delete
	after terminating the editor.

Installation
------------

You place this script in a diretory of your PATH environment variable. You
should prefer the /usr/local/bin to make it useable for all users on your
system.

	sudo install -m 0755 remote-ssh-terminal /usr/local/bin


Usage
-----

	usage: notes [temp|shred|help|-h|--help]

	This script start gvim with a empty notes file.
	Without any parameter, it creates a persistent
	notes file in the basedirectory $BASEDIR.

	temp	The notes file is created in a ramdisk
		and is not persistent


	shred	The notes file is similar to "temp" mode
		but after terminating the gvim, the file
		is shred and delete.

	help,-h,--help
		Show this help and usage

	Dependencies:
		- gvim
		- mktemp
		- shred



