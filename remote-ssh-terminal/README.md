remote-ssh-terminal
===================

This script wraps the terminal program to access remote systems via ssh.
The default terminal is x-terminal-emulator. You should change that by setting
the environment variable XTERM to the terminal program in your shell profile
(e.g ~/.profile,~/.bashrc).

It is designed to make ist easy to use on the shell. It has buildin support for
tsocks (see tsocks support). 

This script is conceptionally a helper script normaly used to link to. This
means, you will create a link named "user@remote.tld" to this script and call
this link(see Extended usage).

The remote-ssh-terminal has buildin support for creating and deleting links.
This works only in a directory, where you have write access(see Installation). 


Installation
------------

This helper script needs to be installed in /usr/local/bin with

	sudo install -m 0755 remote-ssh-terminal /usr/local/bin

Using the link farming capability, you need to link this to your personal ~/bin
directory, which needs to be in your PATH defintion:

	ln -s /usr/local/bin/remote-ssh-terminal ~/bin/remote-ssh-terminal

or shorter

	ln -s /usr/local/bin/remote-ssh-terminal ~/bin/rst


Usage
-----

usage:	remote-ssh-terminal [options] remoteserver
	remoteserver	[username@]servername[:port]
			examples:
				root@jumphost.domain.tld
					connect as user root to server jumphost.domain.tld
				ahost.domain.tld:2210
					connect w/ the actual username to server
					ahost.domain.tld to port 2210
	options
		Symlink handling:
		-a Add remoteserver link to this script
			example:
			-a root@hostname.domain.tld
				Add symlink to this script
			-a ts-root@hostname1.domain.tld
				Add symlink to this script with tsocks support
			-a ts-root@hostname1.domain.tld:2210
				Add symlink to this script with tsocks support
				and connect to ssh port 2210

		-d Delete remoteserver link to this script

		x-terminal handling:
		-c x-terminal class name 
			example:
			-c ssh-tunnel	will set the x-terminal class to "ssh-tunnel" 
		-g Windowgeometry
			examples:
			100x50		100 columns 50 lines
			80x24+1200+100	80 columns 24 lines at pixel position +1200+100

		local options:
		-s Use tsocks as tunnel to proxy/bastionhost ssh server
		-t Force pseudo-terminal allocation

		ssh client options:
		-f Enable ssh agent forwarding
		-n Disable ssh agent forwarding
		-o ssh options. See 'man 5 ssh_config' for detailed information


tsocks support
--------------

This script supports tsock to proxy your ssh connection to a bastion/proxy ssh
server.  Yopu need to install tsocks at first and configure it properly at least
this parameters:

	server = 127.0.0.1
	server_port = 1080

Next you need to configure tho your bastion/proxy ssh server in ~/.ssh/config:

	host jumphost.domain.tld
		DynamicForward	1080
		User		username
		Hostname	jumphost.domain.tld

The DynamicForward must match the server_port in tsocks configuration.

On the daily use you need to start a long running ssh session to
jumphost.domain.tld, which is used to establish the proxy connection via
127.0.0.1:1080.

Every programs network traffic, which is called with tsocks, will tunnel the
connection over the jump host.


Simple usage
------------

Simply use this script calling it with a remoteserver definiton like
"user@server.tld", which establish a ssh connection to server.tld as user
"user":

	remote-ssh-terminal user@server.tld

If you want to use another ssh port(eg: 2222), you should add only a portdefinition to
the remoteserver: 

	remote-ssh-terminal user@server.tld:2222
	
You can use tsock with the option -s, but this need some preperation to work.
See "tsocks support" for further informations about that:

	remote-ssh-terminal -s  user@server.tld:2222 


Extended usage
--------------

For simpler daily usage, this script has buildin support for linkfarmin. With
this you are able to use "user@server.tld" as scriptname. This will help to
minimize the keystrokes at the shell to connect to servers by unsing the
commandline completion.

Calling

	remote-ssh-terminal -a user@server.tld

will create a link to the helperscript in ~/bin with the name "user@server.tld",
which is an executeable script then.

You should simply execute user@server.tld at the shell now and the remolte ssh
session in a new terminal window will open.


To remove an old link, you need to use the -d option instead:

	remote-ssh-terminal -d user@server.tld

This will remove the definition as link

To use tsocks with this functionality, you need to prepend "ts-" in front of
the remoteserver definiton:

	remote-ssh-terminal -a ts-user@server.tld

which will connect via tsock to user@server.tld

