#!/usr/bin/env python3
#
#
#


import sys
import os
from ipaddress import ip_address, IPv4Address, IPv6Address

if len(sys.argv) < 2:
    cmd = os.path.basename(sys.argv[0])

    print("""

    usage: {} [-4|-6] {{ip4 addr|ip6 addr}}

    This command will check for a valid ip address.
    The return value 0 signals a vaild ip address.

    -4      check for ipv4 address only

    -6      check for ipv6 address only


""".format(cmd), file=sys.stderr)
    sys.exit(2)



parm = sys.argv[1]
check_ip6 = False
check_ip4 = False

if parm == "-4":
    check_ip4 = True
    check_ip = sys.argv[2]
elif parm == "-6":
    check_ip6 = True
    check_ip = sys.argv[2]
else:
    check_ip = sys.argv[1]

try:
    ip_addr = ip_address(check_ip)
    if check_ip4 and type(ip_addr) is IPv4Address:
        sys.exit(0)
    elif check_ip4 and type(ip_addr) is not IPv4Address:
        sys.exit(1)
    elif check_ip6 and type(ip_addr) is IPv6Address:
        sys.exit(0)
    elif check_ip6 and type(ip_addr) is not IPv6Address:
        sys.exit(1)
    else:
        sys.exit(0)
except ValueError:
    sys.exit(1)

# vim:ts=4 sts=4 sw=4 tw=80 ai et ft=python:
