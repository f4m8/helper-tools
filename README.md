helper-tools
============

[This git repository](https://codeberg.org/f4m8/helper-tools) held some of my
helper tools, which should also make your shell and commandline life easier.


* mac-conv

	This helps to convert a mac addresses to all known formats. This show
	you the registered owner name of the mac address space

* ping-helper

	As a remote worker, i want to know when a ping target coming up or down.
	It prints per up/down event one line with timestamp and dns query
	results. 

* remote-ssh-terminal

	This script handles, besides the obvious remote terminal windows, the
	link farm handling and helps to connect your remote server with commands
	like 'root@server.domain.tld' 

* notes

	The notes script helps to make small notes and could use as an extend
	clipboard. The script uses gvim as editor frontend. The default mode is
	to save the notes in a base directory with the subdir path YYYY/MM/DD
	The files named "iso8601 date"-<six random characters>.
	
	The other modes are:

	- temp
		Switch the base diretory to the ram disk /dev/shm.

	- shred
		Switch the base diretory to the ram disk /dev/shm and
		shred and delete the notes file after terminating the
		editor

All helpers are license by GPL.

Copyright (C) 2019-2020, Frank Matthieß

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version. This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
details. You should have received a copy of the GNU General Public License along
with this program. If not, see <https://www.gnu.org/licenses/>

