MAC Address converter
=====================

The max address converter converts a mac address to all known
formats.

Addionaly the converter query the ieee oui.txt file to get the registered
manufacturer of the mac address.


Requirements
------------

This script needs the curl binary as download helper for the update.


Installation
------------

Copy the script to your personal ~/bin directory, which should be in your PATH
environment variable.

```shell
install -m 0755 mac-conv ~/bin
```

For a systemwide installation, copy this script to /usr/local/bin

```shell
install -m 0755 mac-conv /usr/local/bin
```

Update the last version of the ieee oui.txt file is only possible as root
user with write access to /usr/local/bin.-

Usage
-----

```shell
usage: mac-converter [mac address|update]

	mac address	de:ad:be:ef:ca:fe, dead-beef-cafe, deadbe.efcafe
	update		Update the current oui.txt file an generater a condensed
				version
```


Example
-------

Convert and query mac addess 0c:c4:7a:f5:02:01:
```shell
$ mac-conv 0c:c4:7a:f5:02:01
0c:c4:7a:f5:02:01	Super Micro Computer, Inc.

0cc47af50201		0CC47AF50201

0c:c4:7a:f5:02:01	0C:C4:7A:F5:02:01
0cc4:7af5:0201		0CC4:7AF5:0201
0cc47a:f50201		0CC47A:F50201

0c-c4-7a-f5-02-01	0C-C4-7A-F5-02-01
0cc4-7af5-0201		0CC4-7AF5-0201
0cc47a-f50201		0CC47A-F50201

0c.c4.7a.f5.02.01	0C.C4.7A.F5.02.01
0cc4.7af5.0201		0CC4.7AF5.0201
0cc47a.f50201		0CC47A.F50201
```

Update IEEE oui.txt as user with sudo permissions for systemwide installation:
```shell
$ sudo mac-conv update
```

